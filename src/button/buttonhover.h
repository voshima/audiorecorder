#ifndef BUTTONHOVER_H
#define BUTTONHOVER_H

#include <QObject>
#include <QEvent>
#include <QPushButton>

class ButtonHoverWatcher : public QObject
{
    Q_OBJECT
public:
    explicit ButtonHoverWatcher(QObject * parent = Q_NULLPTR, QString stat = "", QString H = "");
    virtual bool eventFilter(QObject * watched, QEvent * event) Q_DECL_OVERRIDE;
private:
    QString btnhover;
    QString btnstat;
};

#endif // BUTTONHOVER_H
