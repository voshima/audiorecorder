#include "buttonhover.h"

ButtonHoverWatcher::ButtonHoverWatcher(QObject * parent, QString h, QString stat) :
    QObject(parent)
{
    // for different buttons
    btnstat  = stat;
    btnhover = h;
}

bool ButtonHoverWatcher::eventFilter(QObject * watched, QEvent * event)
{
    QPushButton * button = qobject_cast<QPushButton*>(watched);
    if (!button) {
        return false;
    }

    if (event->type() == QEvent::Enter) {
        // The push button is hovered by mouse
        button->setIcon(QIcon(btnhover));
        return true;
    }

    if (event->type() == QEvent::Leave){
        // The push button is not hovered by mouse
        button->setIcon(QIcon(btnstat));
        return true;
    }

    return false;
}
