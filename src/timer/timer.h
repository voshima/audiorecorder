#include <QString>

namespace timer {
    class time {
    public:
        void update();
        void clear();
        void toNormalFormat(int v, QString& Format);

        int getHours() const { return hours; }
        int getMinutes() const { return min; }
        int getSeconds() const { return sec; }

    private:
        int hours = 0;
        int min = 0;
        int sec = 0;
    };

}
