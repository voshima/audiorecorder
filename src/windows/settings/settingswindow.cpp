#include "settingswindow.h"
#include "ui_settingswindow.h"

#include "../main/mainwindow.h"
#include "src/button/buttonhover.h"

AudioSettingsWindow::AudioSettingsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AudioSettingsWindow)
{
    ui->setupUi(this);

    // set item inputs
    ui->Inputs->addItem(tr("Default"), QVariant(QString()));
    QAudioRecorder* audio = new QAudioRecorder(this);
    input = audio->audioInput();
    foreach (const QString &device, audio->audioInputs()) {
        ui->Inputs->addItem(device, QVariant(device));
    }

    // set item qualities
    ui->Qualities->addItem("Very High");
    ui->Qualities->addItem("High");
    ui->Qualities->addItem("Normal");
    ui->Qualities->addItem("Low");
    ui->Qualities->addItem("Very Low");

    delete audio;

    ui->LocLabel->setText(fileName);

    // fix bug with hovering
    ButtonHoverWatcher * watcher = new ButtonHoverWatcher(this, ":/close_settings/CloseSettingsHover.png", ":/close_settings/CloseSettings.png");
    ui->CloseBtn->installEventFilter(watcher);
}

void AudioSettingsWindow::on_OutputBtn_clicked()
{
    fileName = QFileDialog::getSaveFileName(this, tr("Choose locations for save:"), "record.wav", tr("WAVE (*.wav)"));
    ui->LocLabel->setText(fileName);
    thisWindow->setOutput(fileName);
}

AudioSettingsWindow::~AudioSettingsWindow()
{
    delete ui;
}

void AudioSettingsWindow::on_Inputs_activated(const QString &choosenInput)
{
    input = choosenInput;
    thisWindow->setInput(input);
}

void AudioSettingsWindow::on_Qualities_activated(const QString &choosenQuality)
{
    const std::string CQ = choosenQuality.toUtf8().constData();
    switch (CQ[0]) {
    case 'L': quality = QMultimedia::LowQuality; break;
    case 'N': quality = QMultimedia::NormalQuality; break;
    case 'H': quality = QMultimedia::HighQuality; break;
    default:
        quality = CQ[5] == 'L' ? QMultimedia::VeryLowQuality : QMultimedia::VeryHighQuality;
        break;
    }
    thisWindow->setQuality(quality);
}
